'use strict'

class Timer
{
    constructor()
    {
        this.counter;
    }
    
    set minutes(minutes) {this._minutes = parseInt(minutes) || 0;}
    get minutes() {return this._minutes;}
    
    set seconds(seconds) {this._seconds = parseInt(seconds) || 0;}
    get seconds() {return this._seconds;}

    startTimer(callback)
    {
        // Get duration in seconds
        let remaining = this.toSeconds();

        // Start counter
        if (remaining > 0)
        {
            this.counter = setInterval(() => {

                // X:XX
                if (this.minutes > 0 && this.seconds > 0)
                {
                    this.seconds--;
                }
                // X:00
                else if (this.minutes > 0 && this.seconds <= 0)
                {
                    this.minutes--;
                    this.seconds = 59;
                }
                // 0:XX
                else if (this.minutes <= 0 && this.seconds > 0)
                {
                    this.seconds--;
                    if (this.seconds === 0)
                    {
                        callback(false);
                        this.stopTimer();
                    }
                }
                
                callback(true, this.minutes, this.seconds);
                
                remaining--;
            }, 1000);
        }
    }
    stopTimer()
    {
        clearInterval(this.counter);
    }
    toSeconds()
    {
        return parseInt(this.minutes * 60) + parseInt(this.seconds);
    }
}