'use strict'

class UI
{
    static audio = new Audio('./assets/sounds/done.mp3');
    static showAlert(type, message, appendTo)
    {
        const alert = document.createElement('div');
        alert.classList.add('alert', `alert-${type}`, 'fade', 'show');
        alert.appendChild(document.createTextNode(message));
        const createdAlert = appendTo.appendChild(alert);
        window.setTimeout(() => {
            createdAlert.remove();
        }, 3000);
    }
    static showModal(modalIdentifier)
    {
        document.getElementById(modalIdentifier).classList.remove('d-none');
    }
    static closeModal(modalIdentifier)
    {
        document.getElementById(modalIdentifier).classList.add('d-none');
    }
    static clearInputFields(...elements)
    {
        elements.forEach((element) => {
            element.value = '';
        });
    }
    static updateTimer(...elements)
    {
        elements.forEach((element) => {
            element.target.textContent = element.value > 0 ? element.value.toString().padStart(2, 0) : '00';
        });
    }
    static toggleTimerState(elements)
    {
        // Current state
        const isRunning = document.body.classList.contains('running');

        // Determine current timer state
        if (isRunning)
        {
            // Toggle state
            document.body.classList.remove('running');

            // Show form
            elements.timerForm.classList.remove('d-none');

            // Hide timer
            elements.timerElement.runningTimer.classList.add('d-none');

            // Toggle actions
            elements.actions.classList.remove('d-none');
            elements.actionsRunning.classList.add('d-none');

            // Show presets
            elements.timerPresets.classList.remove('d-none');
        }
        else
        {
            // Toggle state
            document.body.classList.add('running');

            // Hide form
            elements.timerForm.classList.add('d-none');

            // Show timer
            this.updateTimer({
                    target: elements.timerElement.runningTimerMinutes,
                    value: elements.timer.minutes.toString().padStart(2, 0)
                },
                {
                    target: elements.timerElement.runningTimerSeconds,
                    value: elements.timer.seconds.toString().padStart(2, 0)
                }
            );

            elements.timerElement.runningTimer.classList.remove('d-none');

            // Toggle actions
            elements.actions.classList.add('d-none');
            elements.actionsRunning.classList.remove('d-none');

            // Hide presets
            elements.timerPresets.classList.add('d-none');
        }
    }
    static playSound()
    {
        // Loop
        // this.audio.addEventListener('ended', () => {
        //     this.audio.currentTime = 0;
        //     this.audio.play();
        //     console.log('loop');
        // }, false);
        this.audio.play();
    }
    static stopSound()
    {
        this.audio.pause();
        this.audio.currentTime = 0;
    }
}