'use strict'

// Elements
const alertsWrap = document.getElementById('alerts');
const timerForm = document.getElementById('timer-form');
const runningTimer = document.getElementById('running-timer');
const runningTimerMinutes = document.getElementById('running-timer-minutes');
const runningTimerSeconds = document.getElementById('running-timer-seconds');
const minutesInput = document.getElementById('minutes-input');
const secondsInput = document.getElementById('seconds-input');
const actions = document.getElementById('actions');
const actionsRunning = document.getElementById('actions-running');
const timerPresets = document.getElementById('timer-presets');

let timer;

const toggleElements = {
    timerElement: {
        runningTimer,
        runningTimerMinutes,
        runningTimerSeconds
    },
    timerForm,
    actions,
    actionsRunning,
    timerPresets
};

// Start timer
document.querySelectorAll('.start-timer').forEach((element) => {
    element.addEventListener('click', (e) => {

        // Get values
        const isPreset = e.currentTarget.hasAttribute('data-preset') ? JSON.parse(e.currentTarget.dataset.preset) : false;
        const isMute = e.currentTarget.hasAttribute('data-mute') ? JSON.parse(e.currentTarget.dataset.mute) : false;
        const minutes = isPreset ? e.currentTarget.dataset.minutes : minutesInput.value;
        const seconds = isPreset ? e.currentTarget.dataset.seconds : secondsInput.value;
    
        // Start timer
        if (minutes <= 0 && seconds <= 0)
        {
            // Error: Invalid duration passed
            UI.showAlert('danger', 'Please enter a valid duration!', alertsWrap);
        }
        else
        {
            // Timer
            timer = new Timer();
            timer.minutes = minutes;
            timer.seconds = seconds;

            // UI
            toggleElements.timer = timer;
            UI.toggleTimerState(toggleElements);
            UI.clearInputFields(minutesInput, secondsInput);

            timer.startTimer((isRunning, minutes, seconds) => {
                UI.updateTimer({
                        target: runningTimerMinutes,
                        value: minutes
                    },
                    {
                        target: runningTimerSeconds,
                        value: seconds
                    }
                );

                // Timer done
                if ( ! isRunning)
                {
                    // 
                    // Show modal if done
                    UI.showModal('modal-timer-expired');

                    // Play sound?
                    if ( ! isMute)
                    {
                        UI.playSound();
                    }

                    // Toggle timer state
                    UI.toggleTimerState(toggleElements);
                }
            });
        }
    });
});

// Reset timer
document.getElementById('reset-timer').addEventListener('click', (e) => {
    UI.clearInputFields(minutesInput, secondsInput);
});

// Stop timer
document.getElementById('stop-timer').addEventListener('click', (e) => {
    UI.toggleTimerState(toggleElements);
    timer.stopTimer();
});

// Close Modal
document.getElementById('close-modal').addEventListener('click', (e) => {
    UI.stopSound();
    UI.closeModal('modal-timer-expired');
});